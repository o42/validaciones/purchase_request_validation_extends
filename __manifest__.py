# -*- coding: utf-8 -*-
{
    'name': 'Request validaciones extends',
    'version': '1.0.0',
    'summary': """
    Agrega validaciones también a las request order (requisiciones)
    """,
    'category': 'Purchases',
    'author': 'Odoo',
    'support': 'Odoo',
    'website': 'https://odoo.com',
    'description':
        """
Requisition Order Approval Cycle
=============================
This module helps to create multiple custom, flexible and dynamic approval route
for requisition orders based on purchase team settings.

Key Features:

 * This module is a extension for purchase_approval_route
        """,
    'data': [
        'security/ir.model.access.csv',
        'views/purchase_team_views.xml',
        'views/catalog_roles_views.xml',
        'views/purchase_request_views.xml',
        'views/purchase_request_line_quotes_views.xml',
        'wizard/view_purchase_request_line_make_quotes.xml',
        'views/request_line_quotes_settings_views.xml',
        'wizard/purchase_request_line_make_purchase_order.xml',
    ],
    'depends': ['purchase_request', 'extends_purchase_approval_route'],
    'installable': True,
    'auto_install': True,
    'application': False,
}
