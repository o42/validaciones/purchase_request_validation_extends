# -*- coding: utf-8 -*-
from odoo import _, api, fields, models
from odoo.exceptions import UserError, ValidationError


class PurchaseRequestLineMakePurchaseOrder(models.TransientModel):
    _inherit = "purchase.request.line.make.purchase.order"

    @api.model
    def _check_valid_request_line(self, request_line_ids):
        if self.env.context.get('is_quotations'):
            company_id = False
            for line in self.env["purchase.request.line"].browse(request_line_ids):
                if line.request_id.state == "done":
                    raise UserError(_("The purchase has already been completed."))
                if line.request_id.state != "quote":
                    raise UserError(
                        _("Purchase Request %s should be in quote status") % line.request_id.name
                    )
                if line.purchase_state == "done":
                    raise UserError(_("The purchase has already been completed."))
                line_company_id = line.company_id and line.company_id.id or False
                if company_id is not False and line_company_id != company_id:
                    raise UserError(_("You have to select lines from the same company."))
                else:
                    company_id = line_company_id
        else:
            supplier = self.env["purchase.request.line"].browse(request_line_ids).mapped("partner_id")
            if len(supplier) > 1:
                raise ValidationError("The lines must be from the same supplier")
            supplier = self.env["purchase.request.line"].browse(request_line_ids).mapped("currency_item")
            if len(supplier) > 1:
                raise ValidationError("It should be the same currency per line")
            return super(PurchaseRequestLineMakePurchaseOrder, self)._check_valid_request_line(request_line_ids)

    def make_quotes(self):
        res = []
        pr_quotation_obj = self.env["purchase.request.line.quotes"]
        quotation_id = False
        for item in self.item_ids:
            line = item.line_id
            if item.price_unit <= 0.0:
                raise UserError(_("Enter a positive price."))
            quotation_data = self.prepare_quotes(line, item)
            quotation_id = pr_quotation_obj.create(quotation_data)
            if quotation_id:
                res.append(quotation_id.id)
        return {
            "domain": [("id", "in", res)],
            "name": _("Quotations"),
            "view_mode": "tree,form",
            "res_model": "purchase.request.line.quotes",
            "view_id": False,
            "context": False,
            "type": "ir.actions.act_window",
        }

    def prepare_quotes(self, line, item):
        if not self.supplier_id:
            raise UserError(_("Enter a supplier."))
        data = {
            "request_id": line.request_id.id,
            "product_id": line.product_id.id,
            "name": line.name,
            "product_qty": line.product_qty,
            "request_id": line.request_id.id,
            "price_unit": item.price_unit,
            "partner_id": self.supplier_id.id,
            "product_uom_id": item.product_uom_id.id if item.product_uom_id else None,
            "line_id": line.id,
            "currency_item": item.currency_id.id if item.currency_id else None
        }
        return data

    @api.model
    def _prepare_item(self, line):
        res = super(PurchaseRequestLineMakePurchaseOrder, self)._prepare_item(line)
        res["price_unit"] = line.price_unit
        if line.currency_item:
            res["currency_id"] = line.currency_item.id
        return res

    @api.model
    def default_get(self, fields):
        res = super(PurchaseRequestLineMakePurchaseOrder, self).default_get(fields)
        active_model = self.env.context.get("active_model", False)
        request_line_ids = []
        if active_model == "purchase.request.line":
            request_line_ids += self.env.context.get("active_ids", [])
        elif active_model == "purchase.request":
            request_ids = self.env.context.get("active_ids", False)
            request_line_ids += (
                self.env[active_model].browse(request_ids).mapped("line_ids.id")
            )
        if not request_line_ids:
            return res
        request_lines = self.env["purchase.request.line"].browse(request_line_ids)
        supplier_ids = request_lines.mapped("partner_id").ids
        if len(supplier_ids) == 1:
            res["supplier_id"] = supplier_ids[0]
        return res

    @api.model
    def _prepare_purchase_order(self, picking_type, group_id, company, origin):
        res = super(PurchaseRequestLineMakePurchaseOrder, self)._prepare_purchase_order(picking_type, group_id, company, origin)
        currency = self.item_ids.mapped("currency_id")
        if currency:
            res["currency_id"] = currency.id
        return res

    @api.model
    def _prepare_purchase_order_line(self, po, item):
        res = super(PurchaseRequestLineMakePurchaseOrder, self)._prepare_purchase_order_line(po, item)
        res["price_unit"] = item.price_unit
        return res

    @api.onchange("supplier_id")
    def onchange_supplier(self):
        for record in self:
            if record.supplier_id and \
                    record.supplier_id.property_purchase_currency_id \
                    and self.env.context.get('is_quotations'):
                record.item_ids.update({
                    "currency_id": record.supplier_id.property_purchase_currency_id.id
                })


class PurchaseRequestLineMakePurchaseOrderItem(models.TransientModel):
    _inherit = "purchase.request.line.make.purchase.order.item"

    price_unit = fields.Monetary(
        string="Price unit",
        currency_field="currency_id",
    )

    currency_id = fields.Many2one(
        "res.currency",
        string="Currency"
    )
