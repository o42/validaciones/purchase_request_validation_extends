# -*- coding: utf-8 -*-
from odoo import fields, models


class CatalogRoles(models.Model):
    _inherit = "catalog.roles"

    is_request = fields.Boolean(string="is for request?", )
    is_a_quote = fields.Boolean(string="is for quote", )
