# -*- coding: utf-8 -*-
from odoo import api, fields, models, _


class PurchaseRequestLine(models.Model):
    _inherit = "purchase.request.line"

    price_unit = fields.Monetary(
        string="Price unit",
        currency_field="currency_id",
    )

    partner_id = fields.Many2one(
        comodel_name="res.partner",
        string="Supplier",
    )

    currency_item = fields.Many2one(
        "res.currency",
        string="Currency"
    )