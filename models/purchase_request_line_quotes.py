# -*- coding: utf-8 -*-
from odoo import api, fields, models


class PurchaseRequestLineQuotes(models.Model):
    _name = "purchase.request.line.quotes"
    _inherit = "purchase.request.line"
    _description = "Lines for quotes"

    purchase_lines = fields.Many2many(
        relation="purchase_request_purchase_order_quotes_line_rel",
    )

    line_id = fields.Many2one("purchase.request.line", string="Purchase request line")

    state = fields.Selection([
        ("pending", "Pending"),
        ("selected", "Selected"),
        ("refused", "Refused")
    ], default="pending", string="State", )

    def button_approve(self):
        for record in self:
            request_id = record.request_id
            quotation_lines = self.env["purchase.request.line.quotes"].search([
                ("request_id", "=", request_id.id),
                ("product_id", "=", record.product_id.id),
                ("id", "!=", record.id)
            ])
            for quo in quotation_lines:
                quo.state = "refused"
            record.line_id.price_unit = record.price_unit
            record.line_id.partner_id = record.partner_id.id
            record.line_id.product_uom_id = record.product_uom_id.id
            record.line_id.currency_item = record.currency_item.id or None
            record.state = "selected"

    def doto_draft(self):
        self.write({"state": "pending"})
