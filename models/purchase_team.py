# -*- coding: utf-8 -*-
from odoo import api, fields, models


class PurchaseTeam(models.Model):
    _inherit = "purchase.team"

    is_request = fields.Boolean(string="is request order?", )

    @api.onchange("is_request")
    def onchange_is_request(self):
        if self.is_request:
            domain = self.env["catalog.roles"].search([
                ("is_request", "=", True),
                ("active", "=", True)
            ]).mapped("id")
            self.domain_role_ids = [(4, id, 0) for id in domain]
        else:
            self.onchange_type_fill_domain_role_ids()
