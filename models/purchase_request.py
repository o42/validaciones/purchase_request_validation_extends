# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from odoo.exceptions import UserError


class PurchaseRequest(models.Model):
    _inherit = "purchase.request"

    po_order_approval_route = fields.Selection(related='company_id.po_order_approval_route',
                                               string="Use Approval Route", readonly=True)

    team_id = fields.Many2one(
        comodel_name="purchase.team", string="Purchase Team",
        readonly=True, states={'draft': [('readonly', False)], 'sent': [('readonly', False)]}, ondelete="restrict"
    )

    approver_ids = fields.One2many(
        comodel_name="purchase.order.approver", inverse_name="request_id", string="Approvers", readonly=True)

    current_approver = fields.Many2one(
        comodel_name="purchase.order.approver", string="Approver",
        compute="_compute_approver", store=True)

    next_approver = fields.Many2one(
        comodel_name="purchase.order.approver", string="Next Approver",
        compute="_compute_approver")

    is_current_approver = fields.Boolean(
        string="Is Current Approver", compute="_compute_approver"
    )

    lock_amount_total = fields.Boolean(
        string="Lock Amount Total", compute="_compute_approver"
    )

    amount_total = fields.Float(
        string='Total',
        store=True, readonly=True,
        compute='_amount_all',
        tracking=True
    )

    @api.depends('line_ids.price_unit', 'line_ids.product_qty')
    def _amount_all(self):
        for order in self:
            amount_total = 0.0
            for line in order.line_ids:
                amount_total += line.price_unit * line.product_qty
            order.update({
                'amount_total': amount_total,
            })

    state = fields.Selection(selection_add=[
        ('quote', 'Quote'),
    ], ondelete={'quote': 'cascade'})

    line_count_quotes = fields.Integer(
        string="Purchase Request Line count",
        compute="_compute_line_count_quotes",
        readonly=True,
    )

    line_quotes_ids = fields.One2many(
        comodel_name="purchase.request.line.quotes",
        inverse_name="request_id",
        string="Quotes to validate",
    )

    @api.depends("line_quotes_ids")
    def _compute_line_count_quotes(self):
        for rec in self:
            rec.line_count_quotes = len(rec.mapped("line_quotes_ids"))

    def action_view_purchase_request_line_quotes(self):
        action = self.env.ref(
            "purchase_request_validation_extends.purchase_request_line_quotes_form_action"
        ).read()[0]
        lines = self.mapped("line_quotes_ids")
        if lines:
            action["domain"] = [("id", "in", lines.ids)]
        return action

    @api.depends('line_ids.estimated_cost', 'line_ids.product_qty')
    def _amount_all(self):
        for order in self:
            amount = 0.0
            for line in order.line_ids:
                amount += line.estimated_cost * line.product_qty
            order.amount_total = amount

    def validate_quote_state(self):
        for record in self:
            if record.state == "quote":
                total = record.line_count
                total_selected = len(record.line_quotes_ids.filtered(lambda x: x.state == "selected").mapped("state"))
                if total_selected < total:
                    raise UserError("There are some products to create or approved quotes.")
                record.check_quotes_settings()

    def check_quotes_settings(self):
        for line in self.line_ids:
            total = line.price_unit * line.product_qty
            rest = self.env["request.line.quotes.settings"].search([
                ("amount_initial", "<=", total),
                ("amount_end", ">=", total),
                ("currency_id", "=", line.currency_item.id),
                ("active", "=", True)
            ], limit=1)
            total_quotes = len(self.line_quotes_ids.filtered(lambda x: x.line_id == line))
            if rest and total_quotes < rest.quantity:
                raise UserError("The product ( %s ) needs to have at least %s quotes" % (line.product_id.name, rest.quantity))

    def button_approved(self):
        for order in self:
            if not order.team_id:
                # Do default behaviour if PO Team is not set
                super(PurchaseRequest, order).button_approved()
            elif order.current_approver:
                self.validate_quote_state()
                if order.current_approver.user_id == self.env.user or self.env.is_superuser():
                    # If current user is current approver (or superuser) update state as "approved"
                    order.current_approver.state = 'approved'
                    order.message_post(body=_('PR approved by %s') % self.env.user.name)
                    # Check is there is another approver
                    if order.next_approver:
                        # Send request to approve is there is next approver
                        order.send_to_approve()
                        if order.current_approver.role_id.is_a_quote:
                            order.update({"state": "quote"})
                        elif order.state == "quote":
                            order.update({"state": "to_approve"})
                    else:
                        # If there is not next approval, than assume that approval is finished and send notification
                        partner = order.requested_by.partner_id if order.requested_by else order.create_uid.partner_id
                        order.message_post_with_view(
                            'purchase_request_validation_extends.order_approval',
                            composition_mode='mass_mail',
                            partner_ids=[(4, partner.id)],
                            auto_delete=True,
                            auto_delete_message=True,
                            parent_id=False,
                            subtype_id=self.env.ref('mail.mt_note').id)
                        # Do default behaviour to set state as "purchase" and update date_approve
                        return super(PurchaseRequest, order).button_approved()

    def button_to_approve(self):
        for order in self:
            res = super(PurchaseRequest, order).button_to_approve()
            # Generate approval route and send PO to approve
            order.generate_approval_route()
            if order.next_approver:
                # And send request to approve
                order.send_to_approve()
                if order.current_approver.role_id.is_a_quote:
                    order.update({"state": "quote"})
            else:
                # If there are not approvers, do default behaviour and move PO to the "Purchase Order" state
                super(PurchaseRequest, order).button_approved()
        return res

    def generate_approval_route(self):
        """
        Generate approval route for order
        :return:
        """
        for order in self:
            if not order.team_id:
                continue
            if order.approver_ids:
                # reset approval route
                order.approver_ids.unlink()
            for team_approver in order.team_id.approver_ids:
                min_amount = float(team_approver.min_amount,)
                if min_amount > order.amount_total:
                    # Skip approver if Minimum Amount is greater than Total Amount
                    continue
                max_amount = float(team_approver.max_amount)
                if max_amount and max_amount < order.amount_total:
                    # Skip approver if Maximum Amount is set and less than Total Amount
                    continue

                # Add approver to the PO
                self.env['purchase.order.approver'].create({
                    'sequence': team_approver.sequence,
                    'team_id': team_approver.team_id.id,
                    'user_id': team_approver.user_id.id,
                    'role': team_approver.role,
                    'role_id': team_approver.role_id.id,
                    'min_amount': team_approver.min_amount,
                    'max_amount': team_approver.max_amount,
                    'lock_amount_total': team_approver.lock_amount_total,
                    'request_id': order.id,
                    'team_approver_id': team_approver.id,
                })

    @api.depends('approver_ids.state', 'approver_ids.lock_amount_total')
    def _compute_approver(self):
        for order in self:
            if not order.team_id:
                order.next_approver = False
                order.current_approver = False
                order.is_current_approver = False
                order.lock_amount_total = False
                continue
            next_approvers = order.approver_ids.filtered(lambda a: a.state == "to approve")
            order.next_approver = next_approvers[0] if next_approvers else False

            current_approvers = order.approver_ids.filtered(lambda a: a.state == "pending")
            order.current_approver = current_approvers[0] if current_approvers else False

            order.is_current_approver = (order.current_approver and order.current_approver.user_id == self.env.user) \
                                        or self.env.is_superuser()

            order.lock_amount_total = len(
                order.approver_ids.filtered(lambda a: a.state == "approved" and a.lock_amount_total)) > 0

    def send_to_approve(self):
        for order in self:
            if order.state != 'to approve' and not order.team_id:
                continue

            main_error_msg = _("Unable to send approval request to next approver.")
            if order.current_approver:
                reason_msg = _("The order must be approved by %s") % order.current_approver.user_id.name
                raise UserError("%s %s" % (main_error_msg, reason_msg))

            if not order.next_approver:
                reason_msg = _("There are no approvers in the selected PO team.")
                raise UserError("%s %s" % (main_error_msg, reason_msg))
            # use sudo as purchase user cannot update purchase.order.approver
            order.sudo().next_approver.state = 'pending'
            # Now next approver became as current
            current_approver_partner = order.current_approver.user_id.partner_id
            if current_approver_partner not in order.message_partner_ids:
                order.message_subscribe([current_approver_partner.id])
            order.with_user(order.requested_by).message_post_with_view(
                'purchase_request_validation_extends.request_to_approve',
                composition_mode='mass_mail',
                partner_ids=[(4, current_approver_partner.id)],
                auto_delete=True,
                auto_delete_message=True,
                parent_id=False,
                subtype_id=self.env.ref('mail.mt_note').id)

    def button_draft(self):
        res = super(PurchaseRequest, self).button_draft()
        self.mapped("line_quotes_ids").doto_draft()
        return res
