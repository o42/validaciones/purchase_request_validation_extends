# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from odoo.exceptions import UserError


class RequestLineQuotesSettings(models.Model):
    _name = "request.line.quotes.settings"

    active = fields.Boolean(
        string="Active", default=True
    )
    amount_end = fields.Float(
        string='Amount end',
        required=True,
    )
    amount_initial = fields.Float(
        string='Amount initial',
        required=True,
    )
    currency_id = fields.Many2one(
        "res.currency", string="Currency",
        help="Currency used for the supplier"
    )
    quantity = fields.Integer(
        string='Quantity',
        required=True,
        help='Amount of quotes to request'
    )
