# -*- coding: utf-8 -*-
from odoo import fields, models


class PurchaseOrderApprover(models.Model):
    _inherit = "purchase.order.approver"

    request_id = fields.Many2one(
        comodel_name='purchase.request', string='Order', ondelete='cascade')

    order_id = fields.Many2one(
        required=False
    )
